package controllers

import api.{AlbumManager, FolderManager}
import controllers.Upload._
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import play.api.http.MimeTypes
import models.persistence._
import play.api.data.Form
import play.api.data.Forms._


object Application extends Controller {

  def index = Action { implicit request =>
    val albums: List[Album] = AlbumManager.listAlbums
    Ok(views.html.albums(albums))
  }

  case class AlbumDetails(name: String)

  val newAlbumForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(AlbumDetails.apply)(AlbumDetails.unapply)
  )

  def saveAlbum = Action { implicit request =>
    newAlbumForm.bindFromRequest.fold(
      formWithError => {
        Redirect(controllers.routes.Application.index()).flashing("failure" -> "Album létrehozása sikertelen!")
      },
      albumDetails => AlbumManager.createAlbum(albumDetails) match {
        case Left(msg) => Redirect(controllers.routes.Application.index()).flashing("failure" -> msg)
        case Right(_) => Redirect(controllers.routes.Application.index()).flashing("success" -> "Album létrehozása sikeres!")
      }
    )
  }

  def listFolders(albumSecretName: String) = Action { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case Some(album) =>
        val folders: List[Folder] = FolderManager.listFoldersOfAlbum(album)
        Ok(views.html.folders(album, folders))
      case None =>
        NotFound("A keresett album nem található! Nem írtál el valamit?")
    }
  }

  case class FolderDetails(name: String)

  val newFolderForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(FolderDetails.apply)(FolderDetails.unapply)
  )

  def saveFolder(albumSecretName: String) = Action { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case None => NotFound("A keresett album nem található! Nem írtál el valamit?")
      case Some(album) =>
        newFolderForm.bindFromRequest.fold(
          formWithError => {
            Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> "Mappa létrehozása sikertelen!")
          },
          folderDetails => FolderManager.createFolder(album, folderDetails) match {
            case Left(msg) => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> msg)
            case Right(_) => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("success" -> "Mappa létrehozása sikeres!")
          }
        )
    }

  }

  def showFolder(albumSecretName: String, folderStoredName: String) = Action { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case None => NotFound("A keresett album nem található! Nem írtál el valamit?")
      case Some(album) =>
        FolderManager.getFolder(album, folderStoredName) match {
          case Some(folder) =>
            val pictures = FolderManager.getPicturesFromFolder(folder)
            Ok(views.html.showFolder(album, folder, pictures))
          case None => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> "A keresett mappa nem található!")
        }
    }
  }

  def deleteFolder(albumSecretName: String, folderStoredName: String) = Action { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case None => NotFound("A keresett album nem található! Nem írtál el valamit?")
      case Some(album) =>
        FolderManager.getFolder(album, folderStoredName) match {
          case Some(folder) =>
            FolderManager.deleteFolder(folder)
            Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("success" -> "Mappa törölve.")
          case None => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> "A keresett mappa nem található!")
        }
    }
  }

  def uploadIntoFolder(albumSecretName: String, folderStoredName: String)  = Action { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case None => NotFound("A keresett album nem található! Nem írtál el valamit?")
      case Some(album) =>
        FolderManager.getFolder(album, folderStoredName) match {
          case Some(folder) =>
            Ok(views.html.upload(album, folder))
          case None => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> "A keresett mappa nem található!")
        }
    }
  }

  def downloadFolder(albumSecretName: String, folderStoredName: String) = Action { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case None => NotFound("A keresett album nem található! Nem írtál el valamit?")
      case Some(album) =>
        FolderManager.getFolder(album, folderStoredName) match {
          case Some(folder) =>
            FolderManager.zipFolder(folder) match {
              case Left(msg) => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> msg)
              case Right(zipFile) => Ok.sendFile(zipFile, onClose = () => zipFile.delete())
            }
          case None => Redirect(controllers.routes.Application.listFolders(albumSecretName)).flashing("failure" -> "A keresett mappa nem található!")
        }
    }
  }

}
