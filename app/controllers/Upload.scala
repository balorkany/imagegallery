package controllers

import java.io._

import api.{AlbumManager, PictureManager, FolderManager}
import models.persistence.Folder
import play.api.Logger
import play.api.libs.Files.TemporaryFile
import play.api.mvc.{MaxSizeExceeded, _}
import utils._
import play.api.libs.json._
import play.api.libs.functional.syntax._


/**
 * User: Kayrnt
 * Date: 19/10/14
 * Time: 00:04
 */

object Upload extends Controller {

  //we want to parse only up to 100MB
  val multipartMaxLengthParser = parse.maxLength(1024 * 100000, parse.multipartFormData)

  def upload(albumSecretName: String, folderStoredName: String) = Action(multipartMaxLengthParser) { implicit request =>
    AlbumManager.getAlbum(albumSecretName) match {
      case Some(album) =>
        FolderManager.getFolder(album, folderStoredName) match {
          case Some(folder) =>
            request.body.fold(
              maxSizeExceeded => Ok(Json.obj("error" -> "Túl nagy file")),
              multipart => {
                multipart.file("qqfile") match {
                  case None => Ok(Json.obj("error" -> "File nem található"))
                  case Some(filePart) => UploadUtils.validateAndStorePicture(filePart, folder.storedName) match {
                    case Left(msg) => Ok(Json.obj("error" -> msg))
                    case Right(storedName) =>
                      val fileName = multipart.dataParts("qqfilename").headOption match {
                        case None => filePart.filename
                        case Some(filename) => filename
                      }

                      PictureManager.savePictureIntoFolder(folder, storedName, fileName)
                      Ok(Json.obj("success" -> true))
                  }
                }
              }
            )

          case None =>
            Ok(Json.obj("error" -> "Mappa nem található"))
        }
      case None =>
        Ok(Json.obj("error" -> "Album nem található"))
    }

  }

}
