package controllers

import java.io.{FileInputStream, FileNotFoundException}

import api.PictureManager
import models.persistence.Picture
import play.api.mvc.{AnyContent, Action, Controller}
import utils.ImageUtils

object Picture extends Controller {

  final val PicturesBaseFolder = s"${System.getProperty("user.dir")}/tmp/picture"
  final val SlideshowBaseFolder = s"${System.getProperty("user.dir")}/tmp/slideshow"
  final val ThumbnailBaseFolder = s"${System.getProperty("user.dir")}/tmp/thumbnail"

  def at(pictureId: Long): Action[AnyContent] = Action { request =>
    PictureManager.getPicture(pictureId) match {
      case None => NotFound
      case Some(picture) =>
        val folder = picture.folder.toOption.getOrElse(throw new RuntimeException(s"Kép hozzárendelt mappa nélkül! (id:${picture.id})"))
        val filePath = s"$PicturesBaseFolder/${folder.storedName}/${picture.storedName}"

        try {
          val file = new java.io.File(filePath)
          Ok.sendFile(content = file, fileName = _ => picture.name)
        } catch {
          //case _:FileNotFoundException => Redirect(redirect_route).flashing("failure" -> s"A keresett file ($fileName) nem található!")
          case _: Throwable => NotFound //Redirect(redirect_route).flashing("failure" -> s"Váratlan hiba lépett fel!")
        }
    }
  }

  def thumbnail(pictureId: Long): Action[AnyContent] = Action { request =>
    PictureManager.getPicture(pictureId) match {
      case None => NotFound
      case Some(picture) =>
        val folder = picture.folder.toOption.getOrElse(throw new RuntimeException(s"Kép hozzárendelt mappa nélkül! (id:${picture.id})"))
        val filePath = s"$ThumbnailBaseFolder/${folder.storedName}/${picture.storedName}"

        try {
          val file = new java.io.File(filePath)
          Ok.sendFile(content = file, fileName = _ => picture.name)
        } catch {
          //case _:FileNotFoundException => Redirect(redirect_route).flashing("failure" -> s"A keresett file ($fileName) nem található!")
          case _: Throwable => NotFound //Redirect(redirect_route).flashing("failure" -> s"Váratlan hiba lépett fel!")
        }
    }
  }

  def slideshow(pictureId: Long): Action[AnyContent] = Action { request =>
    PictureManager.getPicture(pictureId) match {
      case None => NotFound
      case Some(picture) =>
        val folder = picture.folder.toOption.getOrElse(throw new RuntimeException(s"Kép hozzárendelt mappa nélkül! (id:${picture.id})"))
        val filePath = s"$SlideshowBaseFolder/${folder.storedName}/${picture.storedName}"

        try {
          val file = new java.io.File(filePath)
          Ok.sendFile(content = file, fileName = _ => picture.name)
        } catch {
          //case _:FileNotFoundException => Redirect(redirect_route).flashing("failure" -> s"A keresett file ($fileName) nem található!")
          case _: Throwable => NotFound //Redirect(redirect_route).flashing("failure" -> s"Váratlan hiba lépett fel!")
        }
    }
  }


  def deletePicture(albumSecretName: String, folderStoredName: String, pictureId: Long) = Action { request =>
    //ToDo: ezt majd halkan kell intézni, jquery.post-tal, helyi js törléssel..
    PictureManager.deletePicture(pictureId) match {
      case Left(msg) => Redirect(controllers.routes.Application.showFolder(albumSecretName, folderStoredName)).flashing("failure" -> msg)
      case Right(_) => Redirect(controllers.routes.Application.showFolder(albumSecretName, folderStoredName))
    }
  }

}