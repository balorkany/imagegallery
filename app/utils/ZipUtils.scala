package utils

import java.io.{File, FileOutputStream, FileInputStream, BufferedInputStream}
import java.util.zip.{ZipOutputStream, ZipEntry}

object ZipUtils {

  val Buffer = 2 * 1024

  def zip(out: String, files: Iterable[(String, String)], retainPathInfo: Boolean = false) = {
    var data = new Array[Byte](Buffer)
    val zip = new ZipOutputStream(new FileOutputStream(out))
    files.foreach { case (name, path) =>
      if (!retainPathInfo) {
        val shortName = name.splitAt(name.lastIndexOf(File.separatorChar) + 1)._2
        zip.putNextEntry(new ZipEntry(shortName))
      } else {
        zip.putNextEntry(new ZipEntry(name))
      }
      val in = new BufferedInputStream(new FileInputStream(path), Buffer)
      var b = in.read(data, 0, Buffer)
      while (b != -1) {
        zip.write(data, 0, b)
        b = in.read(data, 0, Buffer)
      }
      in.close()
      zip.closeEntry()
    }
    zip.close()
  }

//ToDo: most nem támogat utf-8 karaktereket.. később ilyet lehetne csinálni:
// http://stackoverflow.com/questions/4034505/apache-commons-ziparchiveoutputstream-breaks-upon-adding-filenames-with-non-asci
// http://stackoverflow.com/questions/4212577/zip-file-created-by-java-doesnt-support-chineseutf-8?noredirect=1&lq=1

}