package utils

import java.sql._

import com.github.aselab.activerecord.ActiveRecordCompanion
import com.github.aselab.activerecord.dsl._
import com.github.aselab.activerecord.squeryl.ExpressionConversion
import com.github.aselab.activerecord.reflections.ReflectionUtil.toReflectable
import com.jolbox.bonecp.BoneCPDataSource
import models.persistence.{DbEntity, Tables}
import org.squeryl.Session
import org.squeryl.dsl.ast.OrderByArg
import play.api.Play.current
import play.api.db.DB


import scala.collection.mutable.ListBuffer


object DBUtils {

  /**
   * Make all update related method call and call update on the object
   */
  def update[T <: DbEntity](newObject: T, originalObject: T): T = {
    newObject.copyRelationships(originalObject.asInstanceOf[newObject.type#T])
    newObject.update(originalObject)
    newObject
  }

  /**
   * Helper method to be able to order the activerecords result by String field names
   *
   * The companion class needs to be passed as a first parameter
   * Maybe this can be improved by getting the companion object from the DbEntity object, but this throws ClassNotFound exception
   *
   * I keep the "improved" non working version commented out
   */
  def toOrderByExpression[T <: DbEntity](companion: ActiveRecordCompanion[T], e: DbEntity, name: String, order: String): ExpressionNode = {
    //def toOrderByExpression(e: DbEntity, name: String, order: String): ExpressionNode = {
    //val companion = e._companion
    //val companion = e.recordCompanion

    val arg = new OrderByArg(new ExpressionConversion(companion.fieldInfo.get(name).get).toExpression(e.getValue[Any](name)))

    order.toLowerCase match {
      case "asc" => arg.asc
      case "desc" => arg.desc
      case _ => throw new IllegalArgumentException("order must be 'asc' or 'desc'")
    }
  }

  /**
   * Executes the given select query and returns a list containing one converted element for each row of the result
   */
  def executeQueryWithParams[T](sql: String, conv: ResultSet => T, setParams: PreparedStatement => Unit): List[T] = {
    Tables.inTransaction{
      val connection = Session.currentSession.connection
      // or val connection: Connection = play.api.db.DB.getConnection("activerecord")
      //PlayConfig.connection ???
      //DefaultConfig. ??
      var stmt: PreparedStatement = null
      try {
        stmt = connection.prepareStatement(sql)

        setParams(stmt)

        val rs = stmt.executeQuery()

        val result: ListBuffer[T] = new ListBuffer
        while (rs.next) {
          result += conv(rs)
        }
        result.toList

      } finally {
        if (stmt != null) {
          stmt.close()
        }
      }
    }
  }

  /**
   * Executes the given update query
   */
  def executeUpdateWithParams[T](sql: String, setParams: PreparedStatement => Unit): Int = {
    Tables.inTransaction{
      val connection = Session.currentSession.connection
      executeUpdateWithParamsWithConnection(sql, connection, setParams)
    }
  }

  def executeUpdateWithParamsWithConnection[T](sql: String, connection: Connection, setParams: PreparedStatement => Unit): Int = {
    var stmt: PreparedStatement = null
    try {
      stmt = connection.prepareStatement(sql)

      setParams(stmt)

      stmt.executeUpdate()

    } finally {
      if (stmt != null) {
        stmt.close()
      }
    }
  }

  /**
   * Executes the given update query
   */
  def executeInsertWithParams[T](sql: String, setParams: PreparedStatement => Unit): Long = {
    Tables.inTransaction{
      val connection = Session.currentSession.connection
      executeInsertWithParamsWithConnection(sql, connection, setParams)
    }
  }

  def executeInsertWithParamsWithConnection[T](sql: String, connection: Connection, setParams: PreparedStatement => Unit): Long = {
    var stmt: PreparedStatement = null
    try {
      stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

      setParams(stmt)

      val affectedRows = stmt.executeUpdate()

      if (affectedRows == 0) {
        throw new SQLException("Creating row failed, no rows affected.");
      }

      val generatedKeys = stmt.getGeneratedKeys()

      if (generatedKeys.next()) {
        generatedKeys.getLong(1);
      } else {
        throw new SQLException("Creating user failed, no ID obtained.");
      }

    } finally {
      if (stmt != null) {
        stmt.close()
      }
    }
  }

  /**
   * Simple monitoring info
   */
  def connectionInfo: String = {
    DB.getDataSource() match {
      case ds: BoneCPDataSource => {
        val pool = ds.getPool
        s"""PartitionCount: ${ds.getPartitionCount}
         MaxConnectionsPerPartition: ${ds.getMaxConnectionsPerPartition}
         TotalLeased: ${ds.getTotalLeased}
         TotalFree: ${pool match { case p: Any => pool.getTotalFree}}
         TotalCreatedConnections: ${pool match { case p: Any => pool.getTotalCreatedConnections}}
        """
      }
      case _ => { "Not a BoneCP datasource." }
    }
  }

}