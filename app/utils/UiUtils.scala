package utils

import models.persistence.Folder
import com.github.aselab.activerecord._
import com.github.aselab.activerecord.dsl._


object UiUtils {
  def getPictureNumber(folder: Folder): Int = {
    folder.pictures.toList.length
  }
}