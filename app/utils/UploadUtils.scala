package utils

import java.util.UUID

import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc._
import play.api.Play.current
import java.io.{FileInputStream, File, IOException}
import org.apache.commons.io
import play.api.Logger

object UploadUtils {
  final val FileSep = "/"
  final val UploadLocation = current.configuration.getString("upload.location")
  final val UploadBaseFolder = s"${System.getProperty("user.dir")}$FileSep..${FileSep}nonpublic"

  final val MaxPictureSizeInKb = current.configuration.getInt("upload.maxPictureSize").getOrElse(102400)
  final val AllowedPictureExtensions = List(
    "jpg", "png", "gif",
    "JPG", "PNG", "GIF"
  )

  final val AllowedVideoExtensions = List(
    "mkv", "flv", "avi", "mov", "wmv", "mp4", "mpg", "mpeg", "3gp", "amv",
    "MKV", "FLV", "AVI", "MOV", "WMV", "MP4", "MPG", "MPEG", "3GP", "AMV"
  )

  final val VideoThumbnailPath = s"${System.getProperty("user.dir")}${FileSep}public${FileSep}img${FileSep}videoicon.png"

  def generateRandomFileName(fileName: String): String = {
    val extension: Option[String] = getFileExtension(fileName)
    UUID.randomUUID() + extension.map(e => s".$e").getOrElse("")
  }

  def getFileFromRequest(key: String)(implicit request: Request[AnyContent]): Option[FilePart[TemporaryFile]] = {
    request.body.asMultipartFormData.getOrElse(throw new RuntimeException("Not multipart form!")).file(key)
  }

  def getFileNameFromPath(fileName: String): Option[String] = {
    fileName.lastIndexOf('/') match {
      case lastDotIndex if lastDotIndex > -1 => Some(fileName.substring(lastDotIndex + 1))
      case _ => None
    }
  }

  def getFileExtension(fileName: String): Option[String] = {
    fileName.lastIndexOf('.') match {
      case lastDotIndex if lastDotIndex > -1 => Some(fileName.substring(lastDotIndex + 1))
      case _ => None
    }
  }

  def getPicturePath(fileName: String, folderName: String): (String, String, String) = {
    val storedFileName = generateRandomFileName(fileName)
    (
      s"${System.getProperty("user.dir")}${FileSep}tmp${FileSep}picture$FileSep$folderName$FileSep$storedFileName",
      s"${System.getProperty("user.dir")}${FileSep}tmp${FileSep}slideshow$FileSep$folderName$FileSep$storedFileName",
      s"${System.getProperty("user.dir")}${FileSep}tmp${FileSep}thumbnail$FileSep$folderName$FileSep$storedFileName"
      )
  }

  def validateAndStorePicture(filePart: FilePart[TemporaryFile], folderStoredName: String): Either[String, String] = {
    val file: File = filePart.ref.file
    val fileName: String = filePart.filename

    if (!file.exists() && !file.isFile) return Left("Nincs feltöltött fájl!")

    val fileExtension = getFileExtension(filePart.filename).getOrElse("")

    if (!(AllowedPictureExtensions.contains(fileExtension) || AllowedVideoExtensions.contains(fileExtension)))
      return Left(s"A feltöltött file kiterjesztése nem megfelelő! Az elfogadott képtípusok: ${AllowedPictureExtensions.mkString(", ")} .")

    val maxSize = MaxPictureSizeInKb
    val size: Long = file.length
    if (size > maxSize * 1024) return Left("A feltöltött kép túl nagy! Maximálisan megengedett méret: " + MaxPictureSizeInKb + " KB")
    //ToDo: Content-type validáció?

    val (fullFilePath, slideshowPath, thumbnailPath) = getPicturePath(fileName, folderStoredName)

    try {
      io.FileUtils.moveFile(file, new File(fullFilePath))
      val storedImage = if(AllowedVideoExtensions.contains(fileExtension)) {
        new File(VideoThumbnailPath)
      } else {
        new File(fullFilePath)
      }

      ImageUtils.createThumbnailAndSlideshow(storedImage, thumbnailPath, slideshowPath)
      // io.FileUtils.moveFile(ImageUtils.createSlideshow(storedImage), new File(slideshowPath))
      // io.FileUtils.moveFile(ImageUtils.createThumbnail(storedImage), new File(thumbnailPath))
      Logger.info(s"storeUploadedFile: File stored to local FS. Filename: $fileName to Target: $fullFilePath")
    } catch {
      case ioe: IOException => {
        Logger.error(s"storeUploadedFile: Error moving file on local FS. Source: $fileName Target: $fullFilePath", ioe)
        throw ioe
      }
    }

    Right(fullFilePath)
  }

}