package utils

import java.io.File
import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.nio.JpegWriter
import net.n12n.exif.{Orientation, JpegMetaData}

object ImageUtils {

  /*def createThumbnail(image: File): File = {
    implicit val writer = JpegWriter().withCompression(50)

    val orientationOption =

    println(orientationOption.map(_.toString))

    Image.fromFile(image).max(200,200).output(new File(s"thumbnail_${image.getName}"))
  }

  def createSlideshow(image: File): File = {
    implicit val writer = JpegWriter().withCompression(50)
    Image.fromFile(image).max(1200,1200).output(new File(s"slideshow_${image.getName}"))
  }*/

  def createThumbnailAndSlideshow(image: File, thumbnailPath: String, slideshowPath: String) = {
    implicit val writer = JpegWriter().withCompression(50)

    val thumbnail = Image.fromFile(image).max(200,200)
    val slideshow = Image.fromFile(image).max(1200,1200)

    thumbnail.output(thumbnailPath)
    slideshow.output(slideshowPath)

    /*JpegMetaData(image.getAbsolutePath).exif.foreach(_.ifds.flatMap(_.attributes).foreach(
      attr => println(s"${attr.tag.name}: ${attr.value}")))

    val (orientedTuhmbnail, orientedSlideshow) = JpegMetaData(image.getAbsolutePath).exif.map(seg => seg.orientation) match {
      case Some(Orientation.TopRight) => 
        (thumbnail.flipX, slideshow.flipX)

      case Some(Orientation.BottomRight) =>
        (thumbnail.rotateRight.rotateRight, slideshow.rotateRight.rotateRight)

      case Some(Orientation.BottomLeft) =>
        (thumbnail.rotateRight.rotateRight.flipX, slideshow.rotateRight.rotateRight.flipX)

      case Some(Orientation.LeftTop) =>
        (thumbnail.rotateRight.flipX, slideshow.rotateRight.flipX)

      case Some(Orientation.RightTop) =>
        (thumbnail, slideshow)

      case Some(Orientation.RightBottom) =>
        (thumbnail.rotateLeft.flipX, slideshow.rotateLeft.flipX)

      case Some(Orientation.LeftBottom) =>
        (thumbnail.rotateLeft, slideshow.rotateLeft)

      case _ =>
        (thumbnail, slideshow)
    }

    orientedTuhmbnail.output(thumbnailPath)
    orientedSlideshow.output(slideshowPath)*/

  }

}