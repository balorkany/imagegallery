package models.persistence

import java.sql.Timestamp

import com.github.aselab.activerecord._
import org.squeryl.adapters._
import org.squeryl.internals.DatabaseAdapter


trait DbEntity extends ActiveRecord {
  type T <: DbEntity
  var createdAt: Timestamp = null
  var updatedAt: Timestamp = null

  override def save() = {
    throw new Exception()
  }

  override def create = {
    this.createdAt = new Timestamp(System.currentTimeMillis)
    this.updatedAt = this.createdAt
    super.create
  }

  override def update = {
    throw new Exception()
  }

  def update(old: DbEntity) {
    this.createdAt = old.createdAt
    this.updatedAt = new Timestamp(System.currentTimeMillis)
    super.update
  }

  // to not be stored in the database we have to keep it private
  private var relationshipsCopied = false

  protected def _copyRelationships(original: T) {}

  def copyRelationships(original: T) {
    if (!this.relationshipsCopied) _copyRelationships(original)
    this.relationshipsCopied = true
  }

}


// needed due to foreign key constraints as the default MySQL adapter doesn't support foreign key constraints,
// only MySQLInnoDBAdapter supports them

class PlayConfig2(override val schema: ActiveRecordTables, overrideSettings: Map[String, Any] = Map()) extends PlayConfig(schema) {
  override def adapter(driverClass: String): DatabaseAdapter = driverClass match {
    case "org.h2.Driver" => new H2Adapter
    case "org.postgresql.Driver" => new PostgreSqlAdapter
    case "net.sourceforge.jtds.jdbc.Driver" => new MSSQLServer
    case "com.ibm.db2.jcc.DB2Driver" => new DB2Adapter
    // only this is what we really want to change here
    case "com.mysql.jdbc.Driver" => new MySQLInnoDBAdapter {
      override def quoteIdentifier(s: String) = "`%s`".format(s)
    }
    case "oracle.jdbc.OracleDriver" => new OracleAdapter
    case "org.apache.derby.jdbc.EmbeddedDriver" => new DerbyAdapter
    case driver => throw ActiveRecordException.unsupportedDriver(driver)
  }
}

trait PlaySupport2 {
  self: ActiveRecordTables =>
  override def loadConfig(config: Map[String, Any]): ActiveRecordConfig =
    new PlayConfig2(self, config)
}

object DbTypes {
  val smallDecimalDbType = "decimal(2,1)"
  val decimalDbType = "decimal(16,2)"
  val smallStringDbType = "varchar(256)"
  val mediumStringDbType = "varchar(1024)"
  val longStringDbType = "varchar(4096)"
  val text = "text"
}