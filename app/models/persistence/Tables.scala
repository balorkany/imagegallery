package models.persistence

import java.sql.Timestamp
import java.util.Date

import models.persistence.DbEntity
import com.github.aselab.activerecord._
import com.github.aselab.activerecord.dsl._

case class Album(override val id: Long = 0L,
                  name: String,
                  secretName: String
                   ) extends DbEntity {
  type T = Album

  lazy val folders = hasMany[Folder]

  protected override def _copyRelationships(original: T): Unit = {}
}

case class Folder(override val id: Long = 0L,
                  storedName: String,
                  name: String,
                  albumId: Long
                   ) extends DbEntity {
  type T = Folder

  lazy val album = belongsTo[Album]
  lazy val pictures = hasMany[Picture]
  
  protected override def _copyRelationships(original: T): Unit = {}
}

case class Picture(override val id: Long = 0L,
                   name: String,
                   storedName: String,
                   folderId: Long
                   ) extends DbEntity {
  type T = Picture
  lazy val folder = belongsTo[Folder]
  
  protected override def _copyRelationships(original: T): Unit = {}
}

object Album extends ActiveRecordCompanion[Album]
object Folder extends ActiveRecordCompanion[Folder]
object Picture extends ActiveRecordCompanion[Picture]

trait TablesTrait extends ActiveRecordTables {

  override def execute(sql: String, logging: Boolean = false) {
    super.execute(sql, logging)
  }

  val album = table[Album]("albums")
  val folder = table[Folder]("folders")
  val picture = table[Picture]("pictures")

  ////////////////////////////////////////////////////////////////////////////////
  // foreign key constraints

  // one-to-many

  val albumToFolder =
    oneToManyRelation(album, folder).via((album, folder) => album.id === folder.albumId)

  val folderToPicture =
    oneToManyRelation(folder, picture).via((folder, picture) => folder.id === picture.folderId)
  
  ////////////////////////////////////////////////////////////////////////////////
  // Property changes from default values

  on(album) { c => declare(
    c.id is autoIncremented,
    c.secretName is unique
  )}

  on(folder) { c => declare(
    c.id is autoIncremented,
    c.storedName is unique
  )}

  on(picture) { a => declare(
    a.id is autoIncremented
  )}
}

object Tables extends TablesTrait with PlaySupport2
