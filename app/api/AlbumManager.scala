package api

import controllers.Application.AlbumDetails
import models.persistence._
import com.github.aselab.activerecord.dsl._
import com.github.aselab.activerecord._

object AlbumManager {

  def listAlbums: List[Album] = {
    Album.all.toList
  }

  def getAlbum(albumSecretName: String): Option[Album] = {
    Album.findBy("secretName", albumSecretName)
  }

  def createAlbum(albumDetails: AlbumDetails): Either[String, Unit] = {
    val albumNameToLowerEnglish = albumDetails.name.toLowerCase map {
      case 'á' => 'a'
      case 'é' => 'e'
      case 'í' => 'i'
      case 'ó' => 'o'
      case 'ö' => 'o'
      case 'ő' => 'o'
      case 'ú' => 'u'
      case 'ü' => 'u'
      case 'ű' => 'u'
      case ' ' => '_'
      case c => c
    }

    val albumSecretName = albumNameToLowerEnglish map {
      case 'o' => '0'
      case 'i' => '1'
      case 's' => '8'
      case c => c
    }

    val newAlbum = Album(0L, albumDetails.name, albumSecretName)

    Tables.inTransaction{
      newAlbum.create
    }

    Right(Unit)
  }
}