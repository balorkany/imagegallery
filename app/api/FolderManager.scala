package api

import com.github.aselab.activerecord.dsl._
import controllers.Application.FolderDetails
import models.persistence._
import java.io.File

import utils.{UploadUtils, ZipUtils}

import scalax.file.Path

object FolderManager {
  final val PicturesBaseFolder = s"${System.getProperty("user.dir")}/tmp/picture"
  final val SlideshowBaseFolder = s"${System.getProperty("user.dir")}/tmp/slideshow"
  final val ThumbnailBaseFolder = s"${System.getProperty("user.dir")}/tmp/thumbnail"

  def listFoldersOfAlbum(album: Album): List[Folder] = {
    album.folders.orderBy(_.name asc).toList
  }

  def getFolder(album: Album, storedName: String): Option[Folder] = {
    album.folders.where(f => f.storedName === storedName).headOption //ToDo: ellenőrizd, hogy a mappafizikailag is létezik-e!!
  }

  def getPicturesFromFolder(folder: Folder): List[Picture] = {
    folder.pictures.toList
  }


  def createFolder(album: Album, folderDetails: FolderDetails): Either[String, Unit] = {
    val storedName = UploadUtils.generateRandomFileName(folderDetails.name).split("-").headOption.getOrElse(
      return Left("Hiba történt a mappa létrehozása közben!")
    )

    val originalPath = Path.fromString(s"$PicturesBaseFolder/$storedName")
    val slideshowPath = Path.fromString(s"$SlideshowBaseFolder/$storedName")
    val thumbnailPath = Path.fromString(s"$ThumbnailBaseFolder/$storedName")

    if(!originalPath.isDirectory) {
      if(originalPath.exists) {
        return Left("Hiba történt a mappa létrehozása közben, próbáld meg újra!")
      } else {
        try{
          originalPath.createDirectory(failIfExists=false)
          slideshowPath.createDirectory(failIfExists=false)
          thumbnailPath.createDirectory(failIfExists=false)
        }
        catch {
          case _:Throwable => return Left("Hiba történt a mappa létrehozása közben!")
        }
      }
    }

    val newFolder: Folder = Folder(
      0L,
      storedName,
      folderDetails.name,
      album.id
    )

    Tables.inTransaction{
      newFolder.create
    }

    Right(Unit)
  }

  def deleteFolder(folder: Folder) = {
    val originalPath = Path.fromString(s"$PicturesBaseFolder/${folder.storedName}")
    val slideshowPath = Path.fromString(s"$SlideshowBaseFolder/${folder.storedName}")
    val thumbnailPath = Path.fromString(s"$ThumbnailBaseFolder/${folder.storedName}")

    if(originalPath.exists){
      originalPath.deleteRecursively(continueOnFailure =  true)
    }

    if(slideshowPath.exists){
      slideshowPath.deleteRecursively(continueOnFailure =  true)
    }

    if(thumbnailPath.exists){
      thumbnailPath.deleteRecursively(continueOnFailure =  true)
    }

    Tables.inTransaction{
      folder.pictures.toList.foreach{ p =>
        p.delete()
      }

      folder.delete()
    }
  }

  def zipFolder(folder: Folder): Either[String, File] = {
    val pictureNamesList = folder.pictures.toList.map(p => (p.name, s"$PicturesBaseFolder/${folder.storedName}/${p.storedName}"))
    if(pictureNamesList.isEmpty) return Left("Nincs kép a mappában")

    val folderZipName = s"${folder.name}.zip"
    ZipUtils.zip(folderZipName, pictureNamesList)
    Right(new File(folderZipName))
  }
}