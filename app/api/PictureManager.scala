package api

import models.persistence.{Folder, Picture, Tables}

import scalax.file.Path

object PictureManager {
  final val PicturesBaseFolder = s"${System.getProperty("user.dir")}/tmp/picture"
  final val SlideshowBaseFolder = s"${System.getProperty("user.dir")}/tmp/slideshow"
  final val ThumbnailBaseFolder = s"${System.getProperty("user.dir")}/tmp/thumbnail"

  def getPicture(pictureId: Long): Option[Picture] = {
    Picture.find(pictureId)
  }

  def getFileNameFromPath(fileName: String): Option[String] = {
    fileName.lastIndexOf('/') match {
      case lastDotIndex if lastDotIndex > -1 => Some(fileName.substring(lastDotIndex + 1))
      case _ => None
    }
  }


  def savePictureIntoFolder(folder: Folder, storedName: String, name: String) = {
    getFileNameFromPath(storedName) match {
      case Some(picStoredName) =>
        val newPicture = Picture(0L, name, picStoredName, folder.id)

        Tables.inTransaction{
          newPicture.create
        }

      case None =>
    }
  }

  def deletePicture(pictureId: Long): Either[String, Unit] = {
    getPicture(pictureId) match {
      case None => Left("Nincs meg a kép")
      case Some(picture) =>
        val folder = picture.folder.toOption.getOrElse(throw new RuntimeException(s"Hiba: kép, hozzárendelt mappa nélkül! id: ${picture.id}"))

        val originalPath = Path.fromString(s"$PicturesBaseFolder/${folder.storedName}/${picture.storedName}")
        originalPath.deleteIfExists()

        val slideshowPath = Path.fromString(s"$SlideshowBaseFolder/${folder.storedName}/${picture.storedName}")
        slideshowPath.deleteIfExists()

        val thumbnailPath = Path.fromString(s"$ThumbnailBaseFolder/${folder.storedName}/${picture.storedName}")
        thumbnailPath.deleteIfExists()

        Tables.inTransaction{
          picture.delete()
        }

        Right(Unit)
    }
  }
}