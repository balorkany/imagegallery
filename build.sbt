import play._
import play.PlayImport._
import play.PlayScala._
import sbt._

name := """image-gallery"""

version := "1.0-SNAPSHOT"

pipelineStages := Seq(uglify) //, cssCompress)

includeFilter in uglify := GlobFilter("*.js")

resolvers ++= Seq(
  DefaultMavenRepository,
  Resolver.sonatypeRepo("public"),
  Resolver.typesafeIvyRepo("releases"),
  Resolver.typesafeRepo("snapshots"),
  Resolver.typesafeIvyRepo("snapshots"),
  "remote snapshot repos" at "http://repo.typesafe.com/typesafe/remote-snapshot-repos",
  "RoundEights" at "http://maven.spikemark.net/roundeights"
)

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  filters,
  ws,
  "org.webjars" % "angularjs" % "1.3.0",
  "org.webjars" % "requirejs" % "2.1.11-1",
  //active record
  "com.github.aselab" %% "scala-activerecord" % "0.3.1",
  "com.github.aselab" %% "scala-activerecord-play2" % "0.3.0",
  "com.github.aselab" %% "scala-activerecord-play2-specs" % "0.3.0" % "test",
  "com.github.aselab" %% "scala-activerecord-specs" % "0.3.0" % "test",
  "mysql" % "mysql-connector-java" % "5.1.34",
  "commons-io" % "commons-io" % "2.4",
  //scala io
  "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.2",
  //scrimage
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.0",
  "com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.0",
  "com.sksamuel.scrimage" %% "scrimage-filters" % "2.1.0",
  //slibexif
  "net.n12n.exif" %% "slibexif" % "0.3.1"
)     

lazy val root = (project in file(".")).enablePlugins(PlayScala)

// pipelineStages := Seq(rjs, digest, gzip)
